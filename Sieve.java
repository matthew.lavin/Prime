import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Sieve {
	public static List<Integer> genSieve(Integer lim) {
		SieveItem sieve[] = new SieveItem[lim - 2];
		for (int i = 0; i < lim - 2; i++)
			sieve[i] = new SieveItem(i + 2, true);
		for (int i = 2; i < Math.sqrt(lim) - 2; i++)
			for (int j = (int) Math.pow(i, 2); j < lim; j += i)
				sieve[j - 2].b = false;
		List<Integer> sieved = new ArrayList<Integer>();
		for (int i = 0; i < lim - 2; i++) {
			if (sieve[i].b)
				sieved.add(sieve[i].i);
		}
		return sieved;
	}
}

