import java.util.ArrayList;
import java.util.List;

public class Prime {
	public static void main(String[] args) {
		int limit = Integer.parseInt(args[0]);
		List<Integer> primes = new ArrayList<Integer>();
		long runtime;
		long start = System.currentTimeMillis();
		for (Integer i = new Integer(1); i <= limit; i += 2)
			if (isPrime(i))
				primes.add(i);
		runtime = System.currentTimeMillis() - start;
		System.out.println(runtime);
		start = System.currentTimeMillis();
		primes = Sieve.genSieve(limit);
		runtime = System.currentTimeMillis() - start;
		System.out.println(runtime);
	}

	private static boolean isPrime(Integer l) {
		if (l < 4)
			return true;
		if (l % 2 == 0)
			return false;
		int start = (int) Math.floor(Math.sqrt(l));
		start -= (start % 2 == 0) ? 1 : 0;
		for (int i = 3; i <= start; i += 2)
			if (l % i == 0) 
				return false;
		return true;
	}
}
